var dataptramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế Phường Xuân Trung",
   "address": "Phường Xuân Trung, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9394105,
   "Latitude": 107.2444751
 },
 {
   "STT": 2,
   "Name": "Trạm y tế Phường Xuân Thanh",
   "address": "Phường Xuân Thanh, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9317797,
   "Latitude": 107.2568919
 },
 {
   "STT": 3,
   "Name": "Trạm y tế Phường Xuân Bình",
   "address": "Phường Xuân Bình, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9315892,
   "Latitude": 107.238565
 },
 {
   "STT": 4,
   "Name": "Trạm y tế Phường Xuân An",
   "address": "Phường Xuân An, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9281854,
   "Latitude": 107.2503855
 },
 {
   "STT": 5,
   "Name": "Trạm y tế Phường Xuân Hoà",
   "address": "Phường Xuân Hoà, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9201945,
   "Latitude": 107.2474303
 },
 {
   "STT": 6,
   "Name": "Trạm y tế Phường Phú Bình",
   "address": "Phường Phú Bình, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9150589,
   "Latitude": 107.2363069
 },
 {
   "STT": 7,
   "Name": "Trạm y tế Xã Bình Lộc",
   "address": "Xã Bình Lộc, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9835555,
   "Latitude": 107.2359278
 },
 {
   "STT": 8,
   "Name": "Trạm y tế Xã Bảo Quang",
   "address": "Xã Bảo Quang, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.979535,
   "Latitude": 107.2726187
 },
 {
   "STT": 9,
   "Name": "Trạm y tế Xã Suối Tre",
   "address": "Xã Suối Tre, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9456269,
   "Latitude": 107.2075388
 },
 {
   "STT": 10,
   "Name": "Trạm y tế Xã Bảo Vinh",
   "address": "Xã Bảo Vinh, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9369869,
   "Latitude": 107.2652964
 },
 {
   "STT": 11,
   "Name": "Trạm y tế Xã Xuân Lập",
   "address": "Xã Xuân Lập, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9092302,
   "Latitude": 107.1779936
 },
 {
   "STT": 12,
   "Name": "Trạm y tế Xã Bàu Sen",
   "address": "Xã Bàu Sen, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9195898,
   "Latitude": 107.2062673
 },
 {
   "STT": 13,
   "Name": "Trạm y tế Xã Bàu Trâm",
   "address": "Xã Bàu Trâm, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9284152,
   "Latitude": 107.2835238
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Xã Xuân Tân",
   "address": "Xã Xuân Tân, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.8957296,
   "Latitude": 107.2266805
 },
 {
   "STT": 15,
   "Name": "Trạm y tế Xã Hàng Gòn",
   "address": "Xã Hàng Gòn, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.8803302,
   "Latitude": 107.2075388
 },
 {
   "STT": 16,
   "Name": "Trạm y tế Phường Trảng Dài",
   "address": "Phường Trảng Dài, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.986029,
   "Latitude": 106.8675482
 },
 {
   "STT": 17,
   "Name": "Trạm y tế Phường Tân Phong",
   "address": "Phường Tân Phong, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9827185,
   "Latitude": 106.8395407
 },
 {
   "STT": 18,
   "Name": "Trạm y tế Phường Tân Biên",
   "address": "Phường Tân Biên, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.970533,
   "Latitude": 106.8932141
 },
 {
   "STT": 19,
   "Name": "Trạm y tế Phường Hố Nai",
   "address": "Phường Hố Nai, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 20,
   "Name": "Trạm y tế Phường Tân Hòa",
   "address": "Phường Tân Hòa, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9668904,
   "Latitude": 106.9079296
 },
 {
   "STT": 21,
   "Name": "Trạm y tế Phường Tân Hiệp",
   "address": "Phường Tân Hiệp, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9656045,
   "Latitude": 106.866728
 },
 {
   "STT": 22,
   "Name": "Trạm y tế Phường Bửu Long",
   "address": "Phường Bửu Long, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9586438,
   "Latitude": 106.8053993
 },
 {
   "STT": 23,
   "Name": "Trạm y tế Phường Tân Tiến",
   "address": "Phường Tân Tiến, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9614615,
   "Latitude": 106.8435458
 },
 {
   "STT": 24,
   "Name": "Trạm y tế Phường Tam Hiệp",
   "address": "Phường Tam Hiệp, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9467204,
   "Latitude": 106.8587246
 },
 {
   "STT": 25,
   "Name": "Trạm y tế Phường Long Bình",
   "address": "Phường Long Bình, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.93909,
   "Latitude": 106.8844884
 },
 {
   "STT": 26,
   "Name": "Trạm y tế Phường Quang Vinh",
   "address": "Phường Quang Vinh, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9561302,
   "Latitude": 106.8139787
 },
 {
   "STT": 27,
   "Name": "Trạm y tế Phường Tân Mai",
   "address": "Phường Tân Mai, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9554003,
   "Latitude": 106.8514721
 },
 {
   "STT": 28,
   "Name": "Trạm y tế Phường Thống Nhất",
   "address": "Phường Thống Nhất, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9522153,
   "Latitude": 106.8335215
 },
 {
   "STT": 29,
   "Name": "Trạm y tế Phường Trung Dũng",
   "address": "Phường Trung Dũng, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9553572,
   "Latitude": 106.8222625
 },
 {
   "STT": 30,
   "Name": "Trạm y tế Phường Tam Hòa",
   "address": "Phường Tam Hòa, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9469591,
   "Latitude": 106.8657572
 },
 {
   "STT": 31,
   "Name": "Trạm y tế Phường Hòa Bình",
   "address": "Phường Hòa Bình, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9498463,
   "Latitude": 106.8112007
 },
 {
   "STT": 32,
   "Name": "Trạm y tế Phường Quyết Thắng",
   "address": "Phường Quyết Thắng, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9444518,
   "Latitude": 106.8222625
 },
 {
   "STT": 33,
   "Name": "Trạm y tế Phường Thanh Bình",
   "address": "Phường Thanh Bình, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9494307,
   "Latitude": 106.8185752
 },
 {
   "STT": 34,
   "Name": "Trạm y tế Phường Bình Đa",
   "address": "Phường Bình Đa, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9355815,
   "Latitude": 106.8614105
 },
 {
   "STT": 35,
   "Name": "Trạm y tế Phường An Bình",
   "address": "Phường An Bình, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9240197,
   "Latitude": 106.8665367
 },
 {
   "STT": 36,
   "Name": "Trạm y tế Phường Bửu Hòa",
   "address": "Phường Bửu Hòa, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.926897,
   "Latitude": 106.8160971
 },
 {
   "STT": 37,
   "Name": "Trạm y tế Phường Long Bình Tân",
   "address": "Phường Long Bình Tân, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9012444,
   "Latitude": 106.8526661
 },
 {
   "STT": 38,
   "Name": "Trạm y tế Phường Tân Vạn",
   "address": "Phường Tân Vạn, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9117711,
   "Latitude": 106.8272458
 },
 {
   "STT": 39,
   "Name": "Trạm y tế Xã Tân Hạnh",
   "address": "Xã Tân Hạnh, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9514129,
   "Latitude": 106.7787053
 },
 {
   "STT": 40,
   "Name": "Trạm y tế Xã Hiệp Hòa",
   "address": "Xã Hiệp Hòa, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.932876,
   "Latitude": 106.8379803
 },
 {
   "STT": 41,
   "Name": "Trạm y tế Xã Hóa An",
   "address": "Xã Hóa An, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9359157,
   "Latitude": 106.8038873
 },
 {
   "STT": 42,
   "Name": "Trạm y tế Xã An Hoà",
   "address": "Xã An Hoà, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8831245,
   "Latitude": 106.8709404
 },
 {
   "STT": 43,
   "Name": "Trạm y tế Xã Tam Phước",
   "address": "Xã Tam Phước, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8631954,
   "Latitude": 106.9241116
 },
 {
   "STT": 44,
   "Name": "Trạm y tế Xã Phước Tân",
   "address": "Xã Phước Tân, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 45,
   "Name": "Trạm y tế Xã Long Hưng",
   "address": "Xã Long Hưng, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.861999,
   "Latitude": 106.8591387
 },
 {
   "STT": 46,
   "Name": "Trạm y tế Thị trấn Gia Ray",
   "address": "Thị trấn Gia Ray, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9259723,
   "Latitude": 107.4055814
 },
 {
   "STT": 47,
   "Name": "Trạm y tế Xã Xuân Bắc",
   "address": "Xã Xuân Bắc, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 11.0355552,
   "Latitude": 107.3206904
 },
 {
   "STT": 48,
   "Name": "Trạm y tế Xã Suối Cao",
   "address": "Xã Suối Cao, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 11.0013148,
   "Latitude": 107.3730563
 },
 {
   "STT": 49,
   "Name": "Trạm y tế Xã Xuân Thành",
   "address": "Xã Xuân Thành, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9943485,
   "Latitude": 107.4264005
 },
 {
   "STT": 50,
   "Name": "Trạm y tế Xã Xuân Thọ",
   "address": "Xã Xuân Thọ, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9591637,
   "Latitude": 107.3494043
 },
 {
   "STT": 51,
   "Name": "Trạm y tế Xã Xuân Trường",
   "address": "Xã Xuân Trường, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9650218,
   "Latitude": 107.4098124
 },
 {
   "STT": 52,
   "Name": "Trạm y tế Xã Xuân Hòa",
   "address": "Xã Xuân Hòa, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.8770877,
   "Latitude": 107.544598
 },
 {
   "STT": 53,
   "Name": "Trạm y tế Xã Xuân Hưng",
   "address": "Xã Xuân Hưng, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.8421826,
   "Latitude": 107.4913489
 },
 {
   "STT": 54,
   "Name": "Trạm y tế Xã Xuân Tâm",
   "address": "Xã Xuân Tâm, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.8666632,
   "Latitude": 107.4440254
 },
 {
   "STT": 55,
   "Name": "Trạm y tế Xã Suối Cát",
   "address": "Xã Suối Cát, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9200551,
   "Latitude": 107.3671431
 },
 {
   "STT": 56,
   "Name": "Trạm y tế Xã Xuân Hiệp",
   "address": "Xã Xuân Hiệp, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9089472,
   "Latitude": 107.3845538
 },
 {
   "STT": 57,
   "Name": "Trạm y tế Xã Xuân Phú",
   "address": "Xã Xuân Phú, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9057042,
   "Latitude": 107.3293656
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Xã Xuân Định",
   "address": "Xã Xuân Định, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.8827103,
   "Latitude": 107.2607289
 },
 {
   "STT": 59,
   "Name": "Trạm y tế Xã Bảo Hoà",
   "address": "Xã Bảo Hoà, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9022012,
   "Latitude": 107.2859484
 },
 {
   "STT": 60,
   "Name": "Trạm y tế Xã Lang Minh",
   "address": "Xã Lang Minh, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.8603549,
   "Latitude": 107.3717237
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Thị trấn Vĩnh An",
   "address": "Thị trấn Vĩnh An, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0859726,
   "Latitude": 107.0421324
 },
 {
   "STT": 62,
   "Name": "Trạm y tế Xã Phú Lý",
   "address": "Xã Phú Lý, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.3203867,
   "Latitude": 107.1631115
 },
 {
   "STT": 63,
   "Name": "Trạm y tế Xã Trị An",
   "address": "Xã Trị An, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0986681,
   "Latitude": 106.9650823
 },
 {
   "STT": 64,
   "Name": "Trạm y tế Xã Tân An",
   "address": "Xã Tân An, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.035617,
   "Latitude": 106.9150047
 },
 {
   "STT": 65,
   "Name": "Trạm y tế Xã Vĩnh Tân",
   "address": "Xã Vĩnh Tân, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0436985,
   "Latitude": 107.0185123
 },
 {
   "STT": 66,
   "Name": "Trạm y tế Xã Bình Lợi",
   "address": "Xã Bình Lợi, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0384694,
   "Latitude": 106.8237374
 },
 {
   "STT": 67,
   "Name": "Trạm y tế Xã Thạnh Phú",
   "address": "Xã Thạnh Phú, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0159709,
   "Latitude": 106.8355372
 },
 {
   "STT": 68,
   "Name": "Trạm y tế Xã Thiện Tân",
   "address": "Xã Thiện Tân, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0180013,
   "Latitude": 106.8767297
 },
 {
   "STT": 69,
   "Name": "Trạm y tế Xã Tân Bình",
   "address": "Xã Tân Bình, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 70,
   "Name": "Trạm y tế Xã Bình Hòa",
   "address": "Xã Bình Hòa, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 10.9884677,
   "Latitude": 106.791291
 },
 {
   "STT": 71,
   "Name": "Trạm y tế Xã Mã Đà",
   "address": "Xã Mã Đà, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.1090006,
   "Latitude": 107.0554207
 },
 {
   "STT": 72,
   "Name": "Trạm y tế Xã Hiếu Liêm",
   "address": "Xã Hiếu Liêm, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.1415802,
   "Latitude": 106.9299579
 },
 {
   "STT": 73,
   "Name": "Trạm y tế Thị trấn Trảng Bom",
   "address": "Thị trấn Trảng Bom, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9513704,
   "Latitude": 107.0126076
 },
 {
   "STT": 74,
   "Name": "Trạm y tế Xã Thanh Bình",
   "address": "Xã Thanh Bình, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 11.0545417,
   "Latitude": 107.0946266
 },
 {
   "STT": 75,
   "Name": "Trạm y tế Xã Cây Gáo",
   "address": "Xã Cây Gáo, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 11.0358645,
   "Latitude": 107.0598491
 },
 {
   "STT": 76,
   "Name": "Trạm y tế Xã Bàu Hàm",
   "address": "Xã Bàu Hàm, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9781733,
   "Latitude": 107.1038123
 },
 {
   "STT": 77,
   "Name": "Trạm y tế Xã Sông Thao",
   "address": "Xã Sông Thao, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9665393,
   "Latitude": 107.0861405
 },
 {
   "STT": 78,
   "Name": "Trạm y tế Xã Sông Trầu",
   "address": "Xã Sông Trầu, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9841681,
   "Latitude": 107.021507
 },
 {
   "STT": 79,
   "Name": "Trạm y tế Xã Đông Hoà",
   "address": "Xã Đông Hoà, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 80,
   "Name": "Trạm y tế Xã Bắc Sơn",
   "address": "Xã Bắc Sơn, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 81,
   "Name": "Trạm y tế Xã Hố Nai 3",
   "address": "Xã Hố Nai 3, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9778806,
   "Latitude": 106.9299579
 },
 {
   "STT": 82,
   "Name": "Trạm y tế Xã Tây Hoà",
   "address": "Xã Tây Hoà, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9460215,
   "Latitude": 107.0541715
 },
 {
   "STT": 83,
   "Name": "Trạm y tế Xã Bình Minh",
   "address": "Xã Bình Minh, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.95339,
   "Latitude": 106.9771827
 },
 {
   "STT": 84,
   "Name": "Trạm y tế Xã Trung Hoà",
   "address": "Xã Trung Hoà, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9450388,
   "Latitude": 107.0623406
 },
 {
   "STT": 85,
   "Name": "Trạm y tế Xã Đồi 61",
   "address": "Xã Đồi 61, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 86,
   "Name": "Trạm y tế Xã Hưng Thịnh",
   "address": "Xã Hưng Thịnh, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.941356,
   "Latitude": 107.0817136
 },
 {
   "STT": 87,
   "Name": "Trạm y tế Xã Quảng Tiến",
   "address": "Xã Quảng Tiến, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9458223,
   "Latitude": 106.9904664
 },
 {
   "STT": 88,
   "Name": "Trạm y tế Xã Giang Điền",
   "address": "Xã Giang Điền, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.912027,
   "Latitude": 106.9860384
 },
 {
   "STT": 89,
   "Name": "Trạm y tế Xã An Viễn",
   "address": "Xã An Viễn, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.8808094,
   "Latitude": 107.0000475
 },
 {
   "STT": 90,
   "Name": "Trạm y tế Xã Gia Tân 1",
   "address": "Xã Gia Tân 1, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0572437,
   "Latitude": 107.1698921
 },
 {
   "STT": 91,
   "Name": "Trạm y tế Xã Gia Tân 2",
   "address": "Xã Gia Tân 2, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0611285,
   "Latitude": 107.1693584
 },
 {
   "STT": 92,
   "Name": "Trạm y tế Xã Gia Tân 3",
   "address": "Xã Gia Tân 3, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0377738,
   "Latitude": 107.172939
 },
 {
   "STT": 93,
   "Name": "Trạm y tế Xã Gia Kiệm",
   "address": "Xã Gia Kiệm, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 94,
   "Name": "Trạm y tế Xã Quang Trung",
   "address": "Xã Quang Trung, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 95,
   "Name": "Trạm y tế Xã Bàu Hàm 2",
   "address": "Xã Bàu Hàm 2, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9331253,
   "Latitude": 107.1369389
 },
 {
   "STT": 96,
   "Name": "Trạm y tế Xã Hưng Lộc",
   "address": "Xã Hưng Lộc, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.939506,
   "Latitude": 107.1026592
 },
 {
   "STT": 97,
   "Name": "Trạm y tế Xã Lộ 25",
   "address": "Xã Lộ 25, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.8704278,
   "Latitude": 107.0952864
 },
 {
   "STT": 98,
   "Name": "Trạm y tế Xã Xuân Thiện",
   "address": "Xã Xuân Thiện, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0364891,
   "Latitude": 107.2370874
 },
 {
   "STT": 99,
   "Name": "Trạm y tế Xã Xuân Thạnh",
   "address": "Xã Xuân Thạnh, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9420082,
   "Latitude": 107.1428293
 },
 {
   "STT": 100,
   "Name": "Trạm y tế Thị trấn Tân Phú",
   "address": "Thị trấn Tân Phú, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 101,
   "Name": "Trạm y tế Xã Dak Lua",
   "address": "Xã Dak Lua, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.5474648,
   "Latitude": 107.3553243
 },
 {
   "STT": 102,
   "Name": "Trạm y tế Xã Nam Cát Tiên",
   "address": "Xã Nam Cát Tiên, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.4190148,
   "Latitude": 107.4612752
 },
 {
   "STT": 103,
   "Name": "Trạm y tế Xã Phú An",
   "address": "Xã Phú An, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.366416,
   "Latitude": 107.4676861
 },
 {
   "STT": 104,
   "Name": "Trạm y tế Xã Núi Tượng",
   "address": "Xã Núi Tượng, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3743935,
   "Latitude": 107.4262813
 },
 {
   "STT": 105,
   "Name": "Trạm y tế Xã Tà Lài",
   "address": "Xã Tà Lài, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3670477,
   "Latitude": 107.3671431
 },
 {
   "STT": 106,
   "Name": "Trạm y tế Xã Phú Lập",
   "address": "Xã Phú Lập, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.362303,
   "Latitude": 107.3934468
 },
 {
   "STT": 107,
   "Name": "Trạm y tế Xã Phú Sơn",
   "address": "Xã Phú Sơn, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3358768,
   "Latitude": 107.5209304
 },
 {
   "STT": 108,
   "Name": "Trạm y tế Xã Phú Thịnh",
   "address": "Xã Phú Thịnh, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3270239,
   "Latitude": 107.3967105
 },
 {
   "STT": 109,
   "Name": "Trạm y tế Xã Thanh Sơn",
   "address": "Xã Thanh Sơn, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 110,
   "Name": "Trạm y tế Xã Phú Trung",
   "address": "Xã Phú Trung, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3154812,
   "Latitude": 107.497265
 },
 {
   "STT": 111,
   "Name": "Trạm y tế Xã Phú Xuân",
   "address": "Xã Phú Xuân, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 112,
   "Name": "Trạm y tế Xã Phú Lộc",
   "address": "Xã Phú Lộc, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2986537,
   "Latitude": 107.4144526
 },
 {
   "STT": 113,
   "Name": "Trạm y tế Xã Phú Lâm",
   "address": "Xã Phú Lâm, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 114,
   "Name": "Trạm y tế Xã Phú Bình",
   "address": "Xã Phú Bình, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2602022,
   "Latitude": 107.5090974
 },
 {
   "STT": 115,
   "Name": "Trạm y tế Xã Phú Thanh",
   "address": "Xã Phú Thanh, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.240525,
   "Latitude": 107.4736016
 },
 {
   "STT": 116,
   "Name": "Trạm y tế Xã Trà Cổ",
   "address": "Xã Trà Cổ, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2504701,
   "Latitude": 107.4345906
 },
 {
   "STT": 117,
   "Name": "Trạm y tế Xã Phú Điền",
   "address": "Xã Phú Điền, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.1983189,
   "Latitude": 107.4499404
 },
 {
   "STT": 118,
   "Name": "Trạm y tế Xã Phước Thiền",
   "address": "Xã Phước Thiền, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 119,
   "Name": "Trạm y tế Xã Long Tân",
   "address": "Xã Long Tân, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 120,
   "Name": "Trạm y tế Xã Đại Phước",
   "address": "Xã Đại Phước, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 121,
   "Name": "Trạm y tế Xã Hiệp Phước",
   "address": "Xã Hiệp Phước, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 122,
   "Name": "Trạm y tế Xã Phú Hữu",
   "address": "Xã Phú Hữu, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 123,
   "Name": "Trạm y tế Xã Phú Hội",
   "address": "Xã Phú Hội, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 124,
   "Name": "Trạm y tế Xã Phú Thạnh",
   "address": "Xã Phú Thạnh, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 125,
   "Name": "Trạm y tế Xã Phú Đông",
   "address": "Xã Phú Đông, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 126,
   "Name": "Trạm y tế Xã Long Thọ",
   "address": "Xã Long Thọ, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 127,
   "Name": "Trạm y tế Xã Vĩnh Thanh",
   "address": "Xã Vĩnh Thanh, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 128,
   "Name": "Trạm y tế Xã Phước Khánh",
   "address": "Xã Phước Khánh, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.6680846,
   "Latitude": 106.8237374
 },
 {
   "STT": 129,
   "Name": "Trạm y tế Xã Phước An",
   "address": "Xã Phước An, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 130,
   "Name": "Trạm y tế Thị trấn Long Thành",
   "address": "Thị trấn Long Thành, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7782801,
   "Latitude": 106.9447146
 },
 {
   "STT": 131,
   "Name": "Trạm y tế Xã An Phước",
   "address": "Xã An Phước, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8355792,
   "Latitude": 106.9417632
 },
 {
   "STT": 132,
   "Name": "Trạm y tế Xã Bình An",
   "address": "Xã Bình An, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8508506,
   "Latitude": 107.0513633
 },
 {
   "STT": 133,
   "Name": "Trạm y tế Xã Long Đức",
   "address": "Xã Long Đức, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8438035,
   "Latitude": 106.9889904
 },
 {
   "STT": 134,
   "Name": "Trạm y tế Xã Lộc An",
   "address": "Xã Lộc An, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8111548,
   "Latitude": 106.9889904
 },
 {
   "STT": 135,
   "Name": "Trạm y tế Xã Bình Sơn",
   "address": "Xã Bình Sơn, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.80272,
   "Latitude": 107.0421324
 },
 {
   "STT": 136,
   "Name": "Trạm y tế Xã Tam An",
   "address": "Xã Tam An, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 137,
   "Name": "Trạm y tế Xã Cẩm Đường",
   "address": "Xã Cẩm Đường, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7827374,
   "Latitude": 107.1071
 },
 {
   "STT": 138,
   "Name": "Trạm y tế Xã Long An",
   "address": "Xã Long An, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7567666,
   "Latitude": 106.9889904
 },
 {
   "STT": 139,
   "Name": "Trạm y tế Xã Suối Trầu",
   "address": "Xã Suối Trầu, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7636602,
   "Latitude": 107.0598491
 },
 {
   "STT": 140,
   "Name": "Trạm y tế Xã Bàu Cạn",
   "address": "Xã Bàu Cạn, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7258801,
   "Latitude": 107.0763453
 },
 {
   "STT": 141,
   "Name": "Trạm y tế Xã Long Phước",
   "address": "Xã Long Phước, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7183848,
   "Latitude": 106.9948945
 },
 {
   "STT": 142,
   "Name": "Trạm y tế Xã Phước Bình",
   "address": "Xã Phước Bình, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 143,
   "Name": "Trạm y tế Xã Tân Hiệp",
   "address": "Xã Tân Hiệp, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.6984529,
   "Latitude": 107.0598491
 },
 {
   "STT": 144,
   "Name": "Trạm y tế Xã Phước Thái",
   "address": "Xã Phước Thái, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.6787052,
   "Latitude": 107.0244171
 },
 {
   "STT": 145,
   "Name": "Trạm y tế Thị trấn Định Quán",
   "address": "Thị trấn Định Quán, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 146,
   "Name": "Trạm y tế Xã Thanh Sơn",
   "address": "Xã Thanh Sơn, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 147,
   "Name": "Trạm y tế Xã Phú Tân",
   "address": "Xã Phú Tân, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.2629318,
   "Latitude": 107.3730563
 },
 {
   "STT": 148,
   "Name": "Trạm y tế Xã Phú Vinh",
   "address": "Xã Phú Vinh, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.2316858,
   "Latitude": 107.3787338
 },
 {
   "STT": 149,
   "Name": "Trạm y tế Xã Phú Lợi",
   "address": "Xã Phú Lợi, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.2236734,
   "Latitude": 107.3907968
 },
 {
   "STT": 150,
   "Name": "Trạm y tế Xã Phú Hòa",
   "address": "Xã Phú Hòa, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.200442,
   "Latitude": 107.4144526
 },
 {
   "STT": 151,
   "Name": "Trạm y tế Xã Ngọc Định",
   "address": "Xã Ngọc Định, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.2016789,
   "Latitude": 107.3021069
 },
 {
   "STT": 152,
   "Name": "Trạm y tế Xã La Ngà",
   "address": "Xã La Ngà, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.1738348,
   "Latitude": 107.2193578
 },
 {
   "STT": 153,
   "Name": "Trạm y tế Xã Gia Canh",
   "address": "Xã Gia Canh, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.1190282,
   "Latitude": 107.4085384
 },
 {
   "STT": 154,
   "Name": "Trạm y tế Xã Phú Ngọc",
   "address": "Xã Phú Ngọc, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.1362281,
   "Latitude": 107.3021069
 },
 {
   "STT": 155,
   "Name": "Trạm y tế Xã Phú Cường",
   "address": "Xã Phú Cường, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.0879316,
   "Latitude": 107.1859936
 },
 {
   "STT": 156,
   "Name": "Trạm y tế Xã Túc Trưng",
   "address": "Xã Túc Trưng, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.1185924,
   "Latitude": 107.2311774
 },
 {
   "STT": 157,
   "Name": "Trạm y tế Xã Phú Túc",
   "address": "Xã Phú Túc, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.0807749,
   "Latitude": 107.2252675
 },
 {
   "STT": 158,
   "Name": "Trạm y tế Xã Suối Nho",
   "address": "Xã Suối Nho, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 159,
   "Name": "Trạm y tế Xã Sông Nhạn",
   "address": "Xã Sông Nhạn, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8422168,
   "Latitude": 107.113007
 },
 {
   "STT": 160,
   "Name": "Trạm y tế Xã Xuân Quế",
   "address": "Xã Xuân Quế, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8612799,
   "Latitude": 107.1602683
 },
 {
   "STT": 161,
   "Name": "Trạm y tế Xã Nhân Nghĩa",
   "address": "Xã Nhân Nghĩa, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8459835,
   "Latitude": 107.2353704
 },
 {
   "STT": 162,
   "Name": "Trạm y tế Xã Xuân Đường",
   "address": "Xã Xuân Đường, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.7946867,
   "Latitude": 107.1839024
 },
 {
   "STT": 163,
   "Name": "Trạm y tế Xã Long Giao",
   "address": "Xã Long Giao, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8274876,
   "Latitude": 107.2282225
 },
 {
   "STT": 164,
   "Name": "Trạm y tế Xã Xuân Mỹ",
   "address": "Xã Xuân Mỹ, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.7740095,
   "Latitude": 107.2607289
 },
 {
   "STT": 165,
   "Name": "Trạm y tế Xã Thừa Đức",
   "address": "Xã Thừa Đức, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.7596614,
   "Latitude": 107.1307289
 },
 {
   "STT": 166,
   "Name": "Trạm y tế Xã Bảo Bình",
   "address": "Xã Bảo Bình, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8358264,
   "Latitude": 107.2935523
 },
 {
   "STT": 167,
   "Name": "Trạm y tế Xã Xuân Bảo",
   "address": "Xã Xuân Bảo, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8622966,
   "Latitude": 107.2797541
 },
 {
   "STT": 168,
   "Name": "Trạm y tế Xã Xuân Tây",
   "address": "Xã Xuân Tây, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8266178,
   "Latitude": 107.3410716
 },
 {
   "STT": 169,
   "Name": "Trạm y tế Xã Xuân Đông",
   "address": "Xã Xuân Đông, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8421101,
   "Latitude": 107.375704
 },
 {
   "STT": 170,
   "Name": "Trạm y tế Xã Sông Ray",
   "address": "Xã Sông Ray, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.7417945,
   "Latitude": 107.3494043
 },
 {
   "STT": 171,
   "Name": "Trạm y tế Xã Lâm San",
   "address": "Xã Lâm San, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.6997304,
   "Latitude": 107.3257545
 }
];